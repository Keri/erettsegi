<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class CalculateController extends Controller
{
    public static function Main()
    {
        $inputs = DataController::getAllExampleData();
        foreach ($inputs as $input) {
            $result = SELF::checkBasicMandatories($input);
            if ($result !== false) {
                $result = SELF::checkLowPoints($input);
            }
            if ($result !== false) {
                $points = SELF::calculateBasicPoints($input);
                print($points . "<br>");
            }
        }
    }
    public static function checkBasicMandatories($inputArray)
    {
        $mandatorySubjects = Config::get("erettsegiszabalyok.kotelezo_targyak");
        foreach ($mandatorySubjects as $mandatorySubject) {
            $key = array_search($mandatorySubject, array_column($inputArray["erettsegi-eredmenyek"], 'nev'));
            if ($key === false) {
                print("Hiba! A tanuló nem érettségizett az összes kötelező tantárgyból! ($mandatorySubject)" . "<br>");
                return false;
            }
        }
    }
    public static function checkLowPoints($inputArray)
    {
        foreach (array_column($inputArray["erettsegi-eredmenyek"], 'eredmeny') as $key => $eredmeny) {
            if ((int)$eredmeny < 20) {
                print('hiba, nem lehetséges a pontszámítás a ' . $inputArray["erettsegi-eredmenyek"][$key]['nev'] . 'ból elért 20% alatti eredmény miatt' . "<br>");
                return false;
            }
        }
    }
    public static function calculateBasicPoints($inputArray)
    {
        $points = 0;
        $plusPoints = 0;
        $targetUniversity = Config::get("erettsegiszabalyok.kovetelmeny_rendszer");
        $key = array_search($inputArray["valasztott-szak"], array_column($targetUniversity, 'minta'));
        if ($key === false) {
            print("Hiba!Nincs rendelkezésre álló szabály erre a szakra! ");
            return false;
        }

        //Kotelezo tantargy Pontjai
        $kotelezo_tantargy = array_search($inputArray["erettsegi-eredmenyek"], array_column($targetUniversity, 'kotelezo_tantargy'));
        $egyetem = $inputArray["valasztott-szak"]["egyetem"];
        $kotelezo_tantargy = $targetUniversity[$egyetem]["szabalyok"]["kotelezo_tantargy"];
        $key = array_search($kotelezo_tantargy, array_column($inputArray["erettsegi-eredmenyek"], 'nev'));
        $kotelezo_adatStruktura = $inputArray["erettsegi-eredmenyek"][$key];
        if ($kotelezo_adatStruktura["tipus"] == "emelt") {
            $plusPoints += 50;
        }
        $points = (int)$kotelezo_adatStruktura["eredmeny"];

        $partsArray = [];
        $partsPlusArray = [];

        //Választható tantargy Pontjai
        $valaszthato_tantargyak = $targetUniversity[$egyetem]["szabalyok"]["valaszthato_tantargy"];
        foreach ($valaszthato_tantargyak as $valaszthato_tantargy) {
            $partPlusPoints = 0;
            $key = array_search($valaszthato_tantargy, array_column($inputArray["erettsegi-eredmenyek"], 'nev'));
            if ($key === false) {
                continue;
            }
            $adatStruktura = $inputArray["erettsegi-eredmenyek"][$key];

            $partPoints = (int)$adatStruktura["eredmeny"];


            if ($adatStruktura["tipus"] == "emelt") {
                $partPlusPoints += 50;
            }
            array_push($partsArray, $partPoints);
            array_push($partsPlusArray, $partPlusPoints);
        }

        $secondPoints = max($partsArray);
        $key = array_search($secondPoints, $partsArray);
        $plusPoints += $partsPlusArray[$key];

        $points = ($points + $secondPoints) * 2;

        //nyelvvizsgaK
        $b2 = array_search("B2", array_column($inputArray["tobbletpontok"], 'tipus'));
        if ($b2 !== false) {

            $plusPoints += 28;
        }
        $c1 = array_search("C1", array_column($inputArray["tobbletpontok"], 'tipus'));
        if ($c1 !== false) {

            $plusPoints += 28;
        }


        if ($plusPoints >= 100) {
            $plusPoints = 100;
        }
        return ($points + $plusPoints);
    }
}
