<?php


return [

    'kotelezo_targyak' => ["magyar nyelv és irodalom", "történelem", "matematika"],
    'tantargy_tipusok' => ["közép", "emelt"],
    'nyelv_vizsga_tipusok_plusz_pont' => ["B2", "C1"],
    'kovetelmeny_rendszer' => ["ELTE"=>[
        "minta" => [
            "egyetem" => "ELTE",
            "kar" => "IK",
            "szak" => "Programtervező informatikus"
        ],
        "szabalyok" => [
            'kotelezo_tantargy' => "matematika", "kotelezo_tantargy_szint" => "közép", 'valaszthato_tantargy' => ["biológia", "fizika", "informatika", "kémia"]
        ]],
        "PPKE"=>["minta" => [
            "egyetem" => "PPKE",
            "kar" => "BTK",
            "szak" => "Anglisztika"
        ],
        "szabalyok" => [
            'kotelezo_tantargy' => "angol", "kotelezo_tantargy_szint" => "emelt", 'valaszthato_tantargy' => ["francia", "német", "olasz", "orosz", "spanyol", "történelem"]
        ]
    ],
],




];
